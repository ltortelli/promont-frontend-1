import { Component, OnInit } from '@angular/core';
import { RestapiService } from 'app/services/restapi.service';
import { DatePipe } from '@angular/common';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BrokerModel } from 'app/models/Broker.model';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dispositivos', title: 'Dispositivos',  icon: 'dvr', class: '' },
    // { path: '/dashboard', title: 'Dashboard',  icon: 'dashboard', class: '' },
    // { path: '/user-profile', title: 'Configurações',  icon: 'person', class: '' },
      // { path: '/user-profile', title: 'Sair',  icon: 'keyboard_arrow_left', class: 'sidenav-footer' },
    // { path: '/typography', title: 'Typography',  icon: 'library_books', class: '' },
     // { path: '/icons', title: 'Icons',  icon: 'keyboard_arrow_left', class: '' },
    // { path: '/notifications', title: 'Notifications',  icon: 'notifications', class: '' },

];
// export const ROUTES: RouteInfo[] = [
//     { path: '/dashboard', title: 'Dashboard',  icon: 'dashboard', class: '' },
//     { path: '/user-profile', title: 'User Profile',  icon: 'person', class: '' },
//     { path: '/table-list', title: 'Table List',  icon: 'content_paste', class: '' },
//     { path: '/typography', title: 'Typography',  icon: 'library_books', class: '' },
//     { path: '/icons', title: 'Icons',  icon: 'bubble_chart', class: '' },
//     { path: '/maps', title: 'Maps',  icon: 'location_on', class: '' },
//     { path: '/notifications', title: 'Notifications',  icon: 'notifications', class: '' },

// ];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  brokers =  new Map();
  destroyed$: ReplaySubject<boolean> = new ReplaySubject(1)

  constructor(
    public datepipe: DatePipe,
    private rest: RestapiService
  ) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.getOnlineBroker()
  }


  getOnlineBroker() {
    this.rest.getApi('broker/').pipe(takeUntil(this.destroyed$)).subscribe(res => {
      if (res != undefined && res.length != 0) {
        console.log(res)
        res.forEach(broker => {
          this.brokers.set(broker.ip, <BrokerModel>broker)
        });
      }
    }, error => {
      console.error('Erro ao buscar periodos: ' + JSON.stringify(error))
    })
  }

  getBrokers() {
    return Array.from(this.brokers.values());
  }

  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
