import { StringifyOptions } from 'querystring';

export class DeviceModel {
  idDevice: number
  serial: string
  idTypeDevice: number
  idBroker: number
  dtUpdate: string
  estadoEixo: string
  gavetaFechada: number
  temperatura: number
  umidade: number
  disjuntor: string
  tensaoEntrada: number
  tensaoBateria: number
  mac: string
  gateway: string
  porta: string
  mascara: string
  IP: string
  online: string
}
